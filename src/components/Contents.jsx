import React from "react";
import { Container, Col, Row, ul, Form } from "react-bootstrap";
import activity from "../images/activity.jpg";
import cenima from "../images/cenima.jpg";
import design from "../images/design.png";

function Contents() {
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <div>
              <h3>ABOUT ME</h3>
              <hr />
              <p>
                In publishing and graphic design, Lorem ipsum is a placeholder
                text commonly used to demonstrate the visual form of a document
                or a typeface without relying on meaningful content. Lorem ipsum
                may be used as a placeholder before final copy is available. It
                is also used to temporarily replace text in a process called
                greeking, which allows designers to consider the form of a
                webpage or publication, without the meaning of the text
                influencing the design.
              </p>
            </div>
            <div>
              <h3>EXPERIENCE</h3>
              <hr />
              <h5>2015 - Present</h5>
              <p style={{ marginLeft: "60px" }}>
                kajkfakjfsjkghskgjhkjfhsjkghauihfsjkghsgfajhjkfhaio
              </p>
            </div>
            <div>
              <h3>TRAINING</h3>
              <hr />
              <div>
                <Container>
                  <Row>
                    <Col>
                      <h5>2016</h5>
                    </Col>
                    <Col xs={8}>
                      <ul>Information Technology</ul>
                      <ul>Information Technology</ul>
                      <ul>Information Technology</ul>
                    </Col>
                    <Col></Col>
                  </Row>
                </Container>
              </div>
            </div>
          </Col>
          <Col>
            <div>
              <h3>EDUACATION</h3>
              <hr />
              <Container>
                <Row>
                  <Col>
                    <h5>2015</h5>
                    <h5>GRAD</h5>
                  </Col>
                  <Col>
                    <h5>Electrical Engineering</h5>
                    <h5>RCC Institute of Information Technology</h5>
                    <p>University</p>
                    <p>DGPA</p>
                  </Col>
                </Row>
              </Container>
            </div>
            <div>
              <Container>
                <Row>
                  <Col>
                    <h5>2015</h5>
                    <h5>GRAD</h5>
                  </Col>
                  <Col>
                    <h5>Electrical Engineering</h5>
                    <h5>RCC Institute of Information Technology</h5>
                    <p>University</p>
                    <p>DGPA</p>
                  </Col>
                </Row>
              </Container>
            </div>
            <div>
              <Container>
                <Row>
                  <Col>
                    <h5>2015</h5>
                    <h5>GRAD</h5>
                  </Col>
                  <Col>
                    <h5>Electrical Engineering</h5>
                    <h5>RCC Institute of Information Technology</h5>
                    <p>University</p>
                    <p>DGPA</p>
                  </Col>
                </Row>
              </Container>
            </div>
            <div>
              <h3>SKILLS</h3>
              <hr />
              <Container>
                <Row>
                  <Col>
                    <ul>
                      CSS
                      {/* <>
                        <Form.Check type="radio" aria-label="radio 2" />
                        <Form.Check type="radio" aria-label="radio 1" />
                        <Form.Check type="radio" aria-label="radio 1" />
                        <Form.Check type="radio" aria-label="radio 1" />
                        <Form.Check type="radio" aria-label="radio 1" />
                      </> */}
                    </ul>
                    <ul>HTML</ul>
                    <ul>JAVA</ul>
                    <ul>Objectives C</ul>
                    <ul>Swift</ul>
                  </Col>
                  <Col>
                    <Form>
                      {["radio"].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                        </div>
                      ))}
                    </Form>
                    <Form>
                      {["radio"].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                        </div>
                      ))}
                    </Form>
                    <Form>
                      {["radio"].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                        </div>
                      ))}
                    </Form>
                    <Form>
                      {["radio"].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                        </div>
                      ))}
                    </Form>
                    <Form>
                      {["radio"].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                          />
                          <Form.Check
                            inline
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                          />
                        </div>
                      ))}
                    </Form>
                  </Col>
                </Row>
              </Container>
            </div>
            <div>
              <h3>INTERESTS</h3>
              <hr />
              <div className={"d-flex"}>
                <Container>
                  <Row>
                    <Col>
                      <div>
                        <img
                          style={{ borderRadius: "50%" }}
                          src={activity}
                          alt=""
                          width="100"
                          height="100"
                        />
                        <h6>Activity</h6>
                      </div>
                    </Col>
                    <Col>
                      <div>
                        <img
                          style={{ borderRadius: "50%" }}
                          src={cenima}
                          alt=""
                          width="100"
                          height="100"
                        />
                        <h6>Cinema</h6>
                      </div>
                    </Col>
                    <Col>
                      <div style={{alignItems: "center"}}>
                        <img
                          style={{ borderRadius: "50%" }}
                          src={design}
                          alt=""
                          width="100"
                          height="100"
                        />
                        <h6>Design</h6>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Contents;

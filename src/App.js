import React, { Component } from 'react';
import Header from './components/Header';
import Contents from './components/Contents';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Contents />
      </div>
    )
  }
}
